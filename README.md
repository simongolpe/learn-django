# Project for learning Django
Project tasks board is [here](https://app.asana.com/0/1201427261754813/1201427261754813) (free alternative to Jira).  

Implemented apps:
- Polls app (django-polls package)
The result of [official Django tutorial](https://docs.djangoproject.com/en/3.2/intro/tutorial01/)
- Photo gallery app
App for creating photo albums, uploading pictures and rendering them.  
Based on [this tutorial](https://engineertodeveloper.com/how-to-build-a-photo-gallery-with-django-part-1/)  
## Getting started
wip