from django.contrib import admin

from photo_gallery.models import Album, Image


class ImageInline(admin.TabularInline):
    model = Image


@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    inlines = [ImageInline]
