from django.shortcuts import render, redirect
from django.forms import modelformset_factory

from photo_gallery.models import Album, Image
from photo_gallery.forms import ImageForm, AlbumForm


def add_album_view(request):
    image_formset = modelformset_factory(Image, form=ImageForm, extra=3)

    if request.method == "GET":
        albums = Album.objects.all()
        album_form = AlbumForm()
        formset = image_formset(queryset=Image.objects.none())
        context = {"albums": albums, "album_form": album_form, "formset": formset}

        return render(request, "photo_gallery/index.html", context)

    album_form = AlbumForm(request.POST)
    formset = image_formset(request.POST, request.FILES)

    if album_form.is_valid() and formset.is_valid():
        album_obj = album_form.save()

        for form in formset.cleaned_data:
            if form:
                image = form["image"]
                Image.objects.create(image=image, album=album_obj)

        return redirect("index")

    print(album_form.errors, formset.errors)


def gallery_view(request, album_name):
    album = Album.objects.get(name=album_name)
    context = {"album": album}

    return render(request, "photo_gallery/gallery.html", context)
