from unittest import skip

from django.test import TestCase
from photo_gallery.models import Album, Image

class AlbumTestCase(TestCase):
    ALBUM_NAME = "2018"

    def setUp(self):
        Album.objects.create(name=self.ALBUM_NAME)

    def test_album_exists(self):
        album_objects = Album.objects.all()
        album_list = [album for album in album_objects]
        album_names = [album.name for album in album_list]

        self.assertIn(self.ALBUM_NAME, album_names)

    @skip("todo")
    def test_album_is_empty(self):
        album_object = Album.objects.get(name=self.ALBUM_NAME)
        images = album_object.image_set

        self.assertEqual(images, "")

class ImageTestCase(TestCase):
    ALBUM_NAME = "2018"

    def setUp(self):
        album = Album.objects.create(name=self.ALBUM_NAME)
        Image.objects.create(image=None, album=album)

    @skip("wip")
    def test_image_exists_in_album(self):
        album = Album.objects.create(name=self.ALBUM_NAME)
        image_object = Image.objects.get(id=1)

        self.assertIn("hoh", image_object.image)
