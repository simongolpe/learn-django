from django.urls import path

from photo_gallery.views import add_album_view, gallery_view

app_name = 'photo_gallery'
urlpatterns = [
    path('<str:album_name>/', gallery_view, name="gallery"),
    path('', add_album_view, name="index"),
]