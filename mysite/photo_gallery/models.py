from django.db import models


class Album(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return str(self.name)


def upload_gallery_image(instance, filename):
    return f"images/{instance.album.name}/gallery/{filename}"


class Image(models.Model):
    image = models.ImageField(upload_to=upload_gallery_image)
    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name="images")
