from django.urls import path

from portal.views import portal_index

app_name = 'portal'
urlpatterns = [
    path('', portal_index, name="index"),
]