from django.shortcuts import render

def portal_index(request):
    return render(request, "portal/index.html")
